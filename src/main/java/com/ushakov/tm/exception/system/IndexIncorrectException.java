package com.ushakov.tm.exception.system;

import com.ushakov.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException() {
        super("Index is not correct!");
    }

    public IndexIncorrectException(String message) {
        super("Entered value " + message + " is not a number!");
    }

}
