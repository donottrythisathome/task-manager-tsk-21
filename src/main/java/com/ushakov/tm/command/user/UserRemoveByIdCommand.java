package com.ushakov.tm.command.user;

import com.ushakov.tm.command.AbstractUserCommand;
import com.ushakov.tm.exception.entity.UserNotFoundException;
import com.ushakov.tm.model.User;
import com.ushakov.tm.util.TerminalUtil;

import java.util.Optional;

public class UserRemoveByIdCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID");
        final String userId = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeOneById(userId);
        Optional.ofNullable(user).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public String name() {
        return "user-remove-by-id";
    }

}
