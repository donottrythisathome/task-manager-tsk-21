package com.ushakov.tm.command;

import com.ushakov.tm.api.service.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    public abstract String name();

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
