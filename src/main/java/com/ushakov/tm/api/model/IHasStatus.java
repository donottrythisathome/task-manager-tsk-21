package com.ushakov.tm.api.model;

import com.ushakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
