package com.ushakov.tm.api.service;

import com.ushakov.tm.model.User;

public interface IAuthService {

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

}
