package com.ushakov.tm.api.service;

import com.ushakov.tm.api.IService;
import com.ushakov.tm.enumerated.Role;
import com.ushakov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User> {

    User add(String login, String password);

    User add(String login, String password, String email);

    User add(String login, String password, Role role);

    User findOneByLogin(String login);

    User removeOneByLogin(String login);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);

}
