package com.ushakov.tm.enumerated;

import com.ushakov.tm.comparator.CreatedComparator;
import com.ushakov.tm.comparator.NameComparator;
import com.ushakov.tm.comparator.StartDateComparator;
import com.ushakov.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", CreatedComparator.getInstance()),
    START_DATE("Sort by start date", StartDateComparator.getInstance()),
    NAME("Sort by name", NameComparator.getInstance()),
    STATUS("Sort by status", StatusComparator.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
